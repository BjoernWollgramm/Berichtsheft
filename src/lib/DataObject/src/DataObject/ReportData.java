package DataObject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ReportData {
    private int reportId;
    private Boolean akzeptiert;
    private LocalDate akzeptiertDate;
    private int berichtswoche;
    private LocalDate erstellungsdatum;
    private LocalDate aenderungsdatum;
    private List<ReportData> reporte;

    public ReportData(){
        reporte = new ArrayList<ReportData>();
    }

    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    public Boolean getAkzeptiert() {
        return akzeptiert;
    }

    public void setAkzeptiert(Boolean akzeptiert) {
        this.akzeptiert = akzeptiert;
    }

    public LocalDate getAkzeptiertDate() {
        return akzeptiertDate;
    }

    public void setAkzeptiertDate(LocalDate akzeptiertDate) {
        this.akzeptiertDate = akzeptiertDate;
    }

    public int getBerichtswoche() {
        return berichtswoche;
    }

    public void setBerichtswoche(int berichtswoche) {
        this.berichtswoche = berichtswoche;
    }

    public LocalDate getErstellungsdatum() {
        return erstellungsdatum;
    }

    public void setErstellungsdatum(LocalDate erstellungsdatum) {
        this.erstellungsdatum = erstellungsdatum;
    }

    public LocalDate getAenderungsdatum() {
        return aenderungsdatum;
    }

    public void setAenderungsdatum(LocalDate aenderungsdatum) {
        this.aenderungsdatum = aenderungsdatum;
    }


}
