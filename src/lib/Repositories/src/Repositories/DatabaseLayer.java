package Repositories;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

public class DatabaseLayer {
    private Connection connection;

    public DatabaseLayer(){
        setDriver();
        setConnection();
    }


    private void setDriver(){


        try {
            Class.forName("org.gjt.mm.mysql.Driver").newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/berichtsheft","root","admin");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
