package Repositories;

import java.util.List;

public abstract class Repository<T> extends DatabaseLayer{

    public abstract int insert(T item);

    public abstract int update(T item, T oldItem);

    public abstract int delete(T item);

    public abstract List<T> getAllItems();

}
